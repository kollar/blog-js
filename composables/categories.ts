let list = reactive([
	{
		id: 1,
		name: "Sport",
	},
	{
		id: 2,
		name: "Culture",
	},
	{
		id: 3,
		name: "Modeling",
	},
	{
		id: 4,
		name: "Programming",
	},
	{
		id: 5,
		name: "Cooking",
	}
])

export const useCategories = () => {
	return {
		list,
	}
}
