let list = reactive([
	{
		id: 1,
		fname: "Michal",
		lname: "Scofield",
		email: "scofield@blog.com",
		password: "secret123",
	},
	{
		id: 2,
		fname: "John",
		lname: "Dean",
		email: "dean@blog.com",
		password: "secret123",
	},
	{
		id: 3,
		fname: "Tom",
		lname: "Brady",
		email: "brady@blog.com",
		password: "secret123",
	},
])
let logged = reactive({
	id: null,
	fname: "",
	lname: "",
	email: "",
	password: ""
})
export const useBlogers = () => {
	const save = ({ fname, lname, email, password }) => {
		let counter = useCounter()
		counter.increment("bloger")
		let id = counter.get("bloger")
		list.push({ id, fname, lname, email, password })
	}
	const signin = ({ email, password }) => {
		let router = useRouter()
		
		for(let bloger of list) {
			if(email == bloger.email && password == bloger.password) {
				let { id, fname, lname, email, password } = bloger
				logged.id = id
				logged.fname = fname
				logged.lname = lname
				logged.email = email
				logged.password = password
			}
			if(logged.id) {
				router.push({ path: "/" })
			}
		}
	}
	const signout = () => {
		logged = null
	}
	return {
		list,
		logged,
		save,
		signin,
		signout
	}
}
