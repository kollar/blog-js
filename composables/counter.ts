let bloger = 3
let article = 12

export const useCounter = () => {
	const increment = (scope) => {
		if(scope == "bloger") {
			bloger++
		}
		else if(scope == "article") {
			article++
		}
	}
	const get = (scope) => {
		if(scope == "bloger") {
			return bloger
		}
		else if(scope == "article") {
			return article
		}
	}
	return {
		bloger,
		article,
		increment,
		get
	}
}
